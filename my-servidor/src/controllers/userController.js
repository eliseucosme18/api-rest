//ficará responsável por lidar com a solicitação do cliente e
//resposta do servidor


const User = require('../models/userModel'); //importa o arquivo userModel para ter acesso a classe usuário

exports.addUser = (req, res) => {//Esta linha exporta a função addUsercomo parte de um objeto que pode ser importado em outros lugares
  const {name} = req.body;//recolhe os dados da solicitação( nome)
  if (!name ) {
    return res.status(400).json({ message: "Nome é obrigatórios." }); //retorna negativo caso esteja vazio
  }

  const newUser = new User(name);//Aqui cria a classe usuario
  User.addUser(newUser);  //a instância recém-criada do usuário ( newUser) foi passada para o método addUser(esse método é o responsável por enviar os dados ao banco)
  res.status(201).json(newUser); //Após adicionar com sucesso o novo usuário, a função responde com um código de status HTTP 201 (Criado)
};

exports.listUsers = (req, res) => {//Esta linha exporta a função listUsers como parte de um objeto que pode ser importado em outros lugares
  const users = User.getAllUsers();
  res.json(users);
};