const express = require('express');
const app = express();
const port = 3000;
const userRoutes = require('./routes/userRoutes'); //está iimportando o módulo, permitindo usar as funções do arquivo router(js)

//para testar esse programa aconselho rodar no postma

// Middleware para processar JSON
app.use(express.json());

// Rota inicial
app.get('/', (req, res) => {
  res.send('Bem-vindo à API de tarefas!');
});
// Rota para as tarefas
app.use('/usuarios', userRoutes);


app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`);
});